﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PanoramaLibrary
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    /// 
    public partial class PLibrary : UserControl
    {
        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        public double MinVisibleX
        {
            get
            {
                double temp = 0;
                DispatchIfNecessary(() =>
                {
                    temp = spControl.MinVisibleX;
                });
                return temp;
            }
        }
        public double MaxVisibleX
        {
            get
            {
                double temp = 0;
                DispatchIfNecessary(() =>
                {
                    temp = spControl.MaxVisibleX;
                });
                return temp;
            }
        }

        public void SetLanguage(string param)
        {
            if (param.ToLower().Contains("ru") || param.ToLower().Contains("en") || param.ToLower().Contains("az"))
            {
                iControl.SetLanguage(param);
                tControl.SetLanguage(param);
                pbControl.SetLanguage(param);
                bControl.SetLanguage(param);
                spControl.SetLanguage(param);
            }
        }

        //Свойства

        private int _ViewMode = 0;
        public int ViewMode
        {
            get { return _ViewMode; }
            set
            {
                if (_ViewMode != value)
                {
                    if (_ViewMode == 4 && isExBearingRequest)
                    {
                        spControl.ExBearingOff();
                    }
                    if (value == 2 && polarChange)
                    {
                        value = 3;
                    }
                    _ViewMode = value;
                    View(_ViewMode);
                    TaskManager();
                }
            }
        }

        private int _Mode = 0;
        public int Mode
        {
            get { return _Mode; }
            set
            {
                if (_Mode != value)
                {
                    _Mode = value;
                    nBar.Mode = value;
                    UpdateRegime(_Mode);
                }
            }
        }

        private int _pbControlVersion = 1;
        public int pbControlVersion
        {
            get { return _pbControlVersion; }
            set
            {
                if (_pbControlVersion != value)
                {
                    _pbControlVersion = value;
                }
            }
        }

        private double pbControlMaxAmplitude()
        {
            switch (pbControlVersion)
            {
                case 0:
                    return _GlobalRangeXmax;
                case 1:
                    return _GlobalRangeXmin + 100 * _GlobalBandWidthMHz;
                default:
                    return _GlobalRangeXmax;
            }
        }

        private double _GlobalRangeXmin = 25.0;
        public double GlobalRangeXmin
        {
            get { return _GlobalRangeXmin; }
            set
            {
                if (_GlobalRangeXmin != value)
                {
                    _GlobalRangeXmin = value;
                    _RecalcGlobalRangeMax();
                    RecalcDivide();

                    iControl.GlobalRangeXmin = value;
                    bControl.GlobalRangeXmin = value;
                    spControl.GlobalRangeXmin = value;

                    //pbControl.MaxAmplitude = _GlobalRangeXmax;
                    pbControl.MaxAmplitude = pbControlMaxAmplitude();
                }
            }
        }

        private void _RecalcGlobalRangeMax()
        {
            _GlobalRangeXmax = _GlobalRangeXmin + _GlobalNumberOfBands * _GlobalBandWidthMHz;
        }

        private double _GlobalRangeXmax = 0;
        private double GlobalRangeXmax
        {
            get { return _GlobalRangeXmax; }
            set
            {
                if (_GlobalRangeXmax != value)
                {
                    _GlobalRangeXmax = value;
                    //!отправить на все контролы
                }
            }
        }

        private double _GlobalRangeYmin = -120;
        public double GlobalRangeYmin
        {
            get { return _GlobalRangeYmin; }
            set
            {
                if (_GlobalRangeYmin != value)
                {
                    _GlobalRangeYmin = value;
                    spControl.GlobalRangeYmin = value;
                }
            }
        }

        private double _GlobalRangeYmax = 0;
        public double GlobalRangeYmax
        {
            get { return _GlobalRangeYmax; }
            set
            {
                if (_GlobalRangeYmax != value)
                {
                    _GlobalRangeYmax = value;
                    spControl.GlobalRangeYmax = value;
                }
            }
        }

        private int _GlobalNumberOfBands = 100;
        public int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                if (_GlobalNumberOfBands != value)
                {
                    _GlobalNumberOfBands = value;
                    _RecalcGlobalRangeMax();
                    RecalcDivide();
                    {
                        spControl.GlobalNumberOfBands = value;
                        bControl.GlobalNumberOfBands = value;
                        iControl.GlobalNumberOfBands = value;

                        //pbControl.MaxAmplitude = _GlobalRangeXmax;
                        pbControl.MaxAmplitude = pbControlMaxAmplitude();
                    }
                    //!отправить на все контролы
                }
            }
        }

        private double _GlobalBandWidthMHz = 30.0;
        public double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                if (_GlobalBandWidthMHz != value)
                {
                    _GlobalBandWidthMHz = value;
                    _RecalcGlobalRangeMax();
                    RecalcDivide();

                    spControl.GlobalBandWidthMHz = value;
                    bControl.GlobalBandWidthMHz = value;
                    iControl.GlobalBandWidthMHz = value;

                    //pbControl.MaxAmplitude = _GlobalRangeXmax;
                    pbControl.MaxAmplitude = pbControlMaxAmplitude();
                }
            }
        }

        private int _DotsPerBand = 9831;
        public int DotsPerBand
        {
            get { return _DotsPerBand; }
            set
            {
                if (_DotsPerBand != value)
                {
                    _DotsPerBand = value;

                    spControl.DotsPerBand = value;
                    iControl.DotsPerBand = value;

                }

            }
        }

        private uint _SavePlotStorageTime = 60;
        public uint SavePlotStorageTime
        {
            get { return SavePlotStorageTime; }
            set
            {
                if (_SavePlotStorageTime != value)
                {
                    _SavePlotStorageTime = value;
                    spControl.SavePlotStorageTime = value;
                }
            }
        }


        //Свойства ЧАП
        private int _MinSpectrValue = -130;
        public int MinSpectrValue
        {
            get { return _MinSpectrValue; }
            set
            {
                if (_MinSpectrValue != value)
                    _MinSpectrValue = value;
                //!учитывать при отрисовке или отправить на SPControl
            }
        }

        private int _SpectrumPointCount = 3000;
        public int SpectrumPointCount
        {
            get { return _SpectrumPointCount; }
            set
            {
                if (_SpectrumPointCount != value)
                {
                    _SpectrumPointCount = value;
                }
            }
        }

        private int _RequestSpectrumTimer = 100;
        public int RequestSpectrumTimer
        {
            get { return _RequestSpectrumTimer; }
            set
            {
                if (_RequestSpectrumTimer != value)
                {
                    _RequestSpectrumTimer = value;
                }
            }
        }

        public delegate void RangeSpectrumXEventHandler(object sender, double MinVisibleX, double MaxVisibleX, int PointCount);

        public event RangeSpectrumXEventHandler NeedSpectrumRequest;

        private bool isSpectrumRequest = false;

        //Таск запроса спектра
        private async void SpectrumRequest()
        {
            while (isSpectrumRequest)
            {
                DispatchIfNecessary(() =>
                {
                    NeedSpectrumRequest?.Invoke(this, spControl.MinVisibleX, spControl.MaxVisibleX, _SpectrumPointCount);
                });
                await Task.Delay(_RequestSpectrumTimer);
            }
            RunSpectrumYet = false;
        }

        private int _CancellationTokenDelay = 1000;
        public int CancellationTokenDelay
        {
            get { return _CancellationTokenDelay; }
            set
            {
                if (_CancellationTokenDelay != value)
                {
                    _CancellationTokenDelay = value;
                }
            }
        }

        //Таск запроса спектра2
        CancellationTokenSource cts = new CancellationTokenSource();
        private async void SpectrumRequest2()
        {
            while (isSpectrumRequest)
            {
                cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;
                DispatchIfNecessary(() =>
                {
                    NeedSpectrumRequest?.Invoke(this, spControl.MinVisibleX, spControl.MaxVisibleX, _SpectrumPointCount);
                });
                try
                {
                    //await Task.Delay(_RequestSpectrumTimer, token);
                    //await Task.Delay(1000, token);

                    //System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                    //stopWatch.Start();
                  
                    //Task<int> task = Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; }, token);

                    var T = await Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; });

                    //var a = await task;

                    //stopWatch.Stop();

                    //TimeSpan ts = stopWatch.Elapsed;
                    //string elapsedTime = String.Format("{0:00}.{1:000}",ts.Seconds, ts.Milliseconds);
                    //Console.WriteLine("RunTime " + elapsedTime);
                }
                catch { }
                await Task.Delay(_RequestSpectrumTimer);
            }
            RunSpectrumYet = false;
        }

        public void Cancel()
        {
            cts.Cancel();
        }


        public enum FramesPerSecond
        {
            FPS25 = 40,
            FPS30 = 33,
            FPS60 = 16
        }
        public void СhooseFPS(FramesPerSecond FPS)
        {
            RequestSpectrumTimer = (int)FPS;
        }

        private int _Threshold = -80;
        public int Threshold
        {
            get { return _Threshold; }
            set
            {
                if (value > 0) value = value * (-1);
                if (_Threshold != value)
                {
                    _Threshold = value;
                    DispatchIfNecessary(() =>
                    {
                        spControl.Threshold = value;
                    });
                }
            }
        }

        private void SpControl_ThresholdChange(int value)
        {
            if (value != _Threshold)
            {
                _Threshold = value;
                ThresholdChange?.Invoke(this, value);
            }
        }

        public delegate void SimpleIntEventHandler(object sender, int value);
        public event SimpleIntEventHandler ThresholdChange;

        public delegate void SimpleEventHandler(object sender);
        public event SimpleEventHandler NeedAdaptiveThresholdRequest;

        public delegate void BandsAdaptiveEventHandler(object sender,  byte[] Bands);
        public event BandsAdaptiveEventHandler NeedAdaptiveThresholdRequest2;

        private int _AdaptiveThreshold = -130;
        public int AdaptiveThreshold
        {
            get { return _AdaptiveThreshold; }
            set
            {
                if (_AdaptiveThreshold != value)
                {
                    if (value > 0) value = (-1) * value;
                    _AdaptiveThreshold = value;
                    spControl.AdaptiveThreshold = value;
                }
            }
        }

        private int _RequestAdaptiveThresholdTimer = 1000;
        public int RequesAdaptiveThresholdtTimer
        {
            get { return _RequestAdaptiveThresholdTimer; }
            set
            {
                if (_RequestAdaptiveThresholdTimer != value)
                {
                    _RequestAdaptiveThresholdTimer = value;
                }
            }
        }

        private bool isAdaptiveThresholdRequest = false;

        //Таск запроса адаптивного порога
        private async void AdaptiveThresholdRequest()
        {
            while (isAdaptiveThresholdRequest)
            {
                DispatchIfNecessary(() =>
                {
                    NeedAdaptiveThresholdRequest?.Invoke(this);
                    NeedAdaptiveThresholdRequest2?.Invoke(this, GetRIBands());
                });
                await Task.Delay(_RequestAdaptiveThresholdTimer);
            }
        }

        private byte[] GetRIBands()
        {
            byte[] Bands = new byte[RIBandsRow.Count()];
            for (int i = 0; i < Bands.Count(); i++)
            {
                Bands[i] = (byte)RIBandsRow[i];
            }
            return Bands;
        }

        public void AdaptiveBandsPaint(byte[] Bands, byte[] Thresholds)
        {
            DispatchIfNecessary(() =>
            {
                spControl.AdaptiveBandsPaint(Bands, Thresholds);
            });
        }


        public event SimpleEventHandler NeedScanSpeedRequest;

        public bool FlagScanSpeed = true;
        public bool AutoGenerateScanSpeed = false;

        private double _ScanSpeed = 2.0;
        public double ScanSpeed
        {
            get => _ScanSpeed;
            set
            {
                if (_ScanSpeed != value)
                {
                    _ScanSpeed = value;
                    DispatchIfNecessary(() =>
                    {
                        spControl.ScanSpeed = value;
                    });
                }
            }
        }
        private double _ScanSpeed2 = 2.0;
        public double ScanSpeed2
        {
            get => _ScanSpeed2;
            set
            {
                if (_ScanSpeed2 != value)
                {
                    _ScanSpeed2 = value;
                    DispatchIfNecessary(() =>
                    {
                        spControl.ScanSpeed2 = value;
                    });
                }
            }
        }

        private int _RequestScanSpeedTimer = 1000;
        public int RequestScanSpeedTimer
        {
            get { return _RequestScanSpeedTimer; }
            set
            {
                if (_RequestScanSpeedTimer != value)
                {
                    _RequestScanSpeedTimer = value;
                }
            }
        }

        private bool isScanSpeedRequest = false;

        //Таск запроса скорости обзора
        private async void ScanSpeedRequest()
        {
            while (isScanSpeedRequest)
            {
                if (AutoGenerateScanSpeed)
                {
                    ScanSpeed = GenerateScanSpeed(ScanSpeed);
                    ScanSpeed2 = GenerateScanSpeed(ScanSpeed2);
                }
                else
                {
                    NeedScanSpeedRequest?.Invoke(this);
                }
                await Task.Delay(_RequestScanSpeedTimer);
            }
            RunScanSpeedYet = false;
            DispatchIfNecessary(() =>
            {
                spControl.ScanSpeedVisible = false;
            });
        }

        private double GenerateScanSpeed(double scanSpeed)
        {
            double ss = scanSpeed;
            if (_Mode == 1)
            {
                Random random = new Random();
                double d = random.NextDouble();
                ss = 10.0 + d;
            }
            if (_Mode == 2)
            {
                Random random = new Random();
                double d = random.NextDouble();
                if (d >= 0 && d < 1 / 3d) ss = 2.1;
                if (d >= 1 / 3d && d < 2 / 3d) ss = 2.2;
                if (d >= 2 / 3d && d <= 1d) ss = 2.3;
            }
            return ss;
        }

        //Свойства ЧПлП
        public delegate void RangeXEventHandler(object sender, double MinVisibleX, double MaxVisibleX);
        public event RangeXEventHandler NeedBearingRequest;

        private int _BearingLiveTime = 3000;
        public int BearingLiveTime
        {
            get { return bControl.LiveTime; }
            set
            {
                if (_BearingLiveTime != value)
                {
                    _BearingLiveTime = value;
                    bControl.LiveTime = value;
                }
            }
        }

        private int _RequestBearingTimer = 100;
        public int RequestBearingTimer
        {
            get { return _RequestBearingTimer; }
            set
            {
                if (_RequestBearingTimer != value)
                {
                    _RequestBearingTimer = value;
                }
            }
        }

        private bool isBearingRequest = false;

        //Таск запроса пеленгов
        private async void BearingRequest()
        {
            while (isBearingRequest)
            {
                DispatchIfNecessary(() =>
                {
                    NeedBearingRequest?.Invoke(this, bControl.MinVisibleX, bControl.MaxVisibleX);
                });
                await Task.Delay(_RequestBearingTimer);
            }
        }

        //Свойства ПЧПлП
        private event SimpleEventHandler NeedPolarBearingRequest;

        private int _PolarBearingLiveTime = 3000;
        public int PolarBearingLiveTime
        {
            get { return pbControl.LiveTime; }
            set
            {
                if (_PolarBearingLiveTime != value)
                {
                    _PolarBearingLiveTime = value;
                    pbControl.LiveTime = value;
                }
            }
        }

        bool polarChange = false;
        private void SpControl_PolarChange(bool value)
        {
            polarChange = value;
            if (ViewMode == 2 && value)
            {
                ViewMode = 3;
            }
            if (ViewMode == 3 && !value)
            {
                ViewMode = 2;
            }
        }

        private int _RequestPolarBearingTimer = 100;
        private int RequestPolarBearingTimer
        {
            get { return _RequestPolarBearingTimer; }
            set
            {
                if (_RequestPolarBearingTimer != value)
                {
                    _RequestPolarBearingTimer = value;
                }
            }
        }

        private bool isPolarBearingRequest = false;

        //Таск запроса полярных пеленгов
        private async void PolarBearingRequest()
        {
            while (isPolarBearingRequest)
            {
                DispatchIfNecessary(() =>
                {
                    NeedPolarBearingRequest?.Invoke(this);
                });
                await Task.Delay(_RequestPolarBearingTimer);
            }
        }

        //Свойства ЧВП
        public delegate void IntensityRangeXEventHandler(object sender, double MinVisibleX, double MaxVisibleX, int PointCount, byte TimeLenght);
        public event IntensityRangeXEventHandler NeedIntensityRequest;

        private int _IntensityPointCount = 2000;
        public int IntensityPointCount
        {
            get { return _IntensityPointCount; }
            set
            {
                if (_IntensityPointCount != value)
                {
                    _IntensityPointCount = value;
                    //! дичь с количеством точек в iControl
                }
            }
        }

        private int _RequestIntensityTimer = 1000;
        public int RequestIntensityTimer
        {
            get { return _RequestIntensityTimer; }
            set
            {
                if (_RequestIntensityTimer != value)
                {
                    _RequestIntensityTimer = value;
                }
            }
        }

        private bool isIntensityRequest = false;

        //Таск запроса временной панорамы
        private async void IntensityRequest()
        {
            while (isIntensityRequest)
            {
                DispatchIfNecessary(() =>
                {
                    NeedIntensityRequest?.Invoke(this, iControl.MinVisibleX, iControl.MaxVisibleX, _IntensityPointCount, 60);
                });
                await Task.Delay(_RequestIntensityTimer);
            }
        }

        private int _CancellationTokenIntensityDelay = 3000;
        public int CancellationTokenIntensityDelay
        {
            get { return _CancellationTokenIntensityDelay; }
            set
            {
                if (_CancellationTokenIntensityDelay != value)
                {
                    _CancellationTokenIntensityDelay = value;
                }
            }
        }

        CancellationTokenSource ctsI = new CancellationTokenSource();
        private async void IntensityRequest2()
        {
            while (isIntensityRequest)
            {
                ctsI = new CancellationTokenSource();
                CancellationToken token = ctsI.Token;
                DispatchIfNecessary(() =>
                {
                    NeedIntensityRequest?.Invoke(this, iControl.MinVisibleX, iControl.MaxVisibleX, _IntensityPointCount, 60);
                });
                try
                {
                    var T = await Task.Delay(_CancellationTokenIntensityDelay, token).ContinueWith(_ => { return 42; });
                }
                catch { }
                await Task.Delay(_RequestIntensityTimer);
            }
            RunIntensityYet = false;
        }

        public void CancelIntensity()
        {
            ctsI.Cancel();
        }


        //Св-ва исполнительного пеленгования
        public delegate void BearingBandXEventHandler(object sender, double MinBandX, double MaxBandX, int PhAvCount, int PlAvCount);
        public event BearingBandXEventHandler NeedExBearingRequest;
        public event BearingBandXEventHandler NeedQBearingRequest;

        private void SpControl_ExBearingChange(bool value)
        {
            if (value)
            {
                if (_Mode == 1 || _Mode == 2)
                {
                    if (_ViewMode != 4)
                    {
                        ViewMode = 4;
                        OnIsNeedChangeView?.Invoke(this, ViewMode);
                    }
                    isExBearingRequest = true;
                    Task.Run(() => ExBearingRequest());
                }
                else
                {
                    isExBearingRequest = false;
                    spControl.ExBearingOff();
                }
            }
            else
            {
                isExBearingRequest = false;
                spControl.ExBearingOff();
            }
        }


        private int _RequestExBearingTimer = 500;
        public int RequestExBearingTimer
        {
            get { return _RequestExBearingTimer; }
            set
            {
                if (_RequestExBearingTimer != value)
                {
                    _RequestExBearingTimer = value;
                }
            }
        }

        private bool isExBearingRequest = false;

        //Таск запроса исполнительного пеленгования
        private async void ExBearingRequest()
        {
            while (isExBearingRequest)
            {
                DispatchIfNecessary(() =>
                {
                    NeedExBearingRequest?.Invoke(this, startExBearingFreq, endExBearingFreq, tControl.AvPhases, tControl.AvBearings);
                });
                await Task.Delay(_RequestExBearingTimer);
            }
        }

        public PLibrary()
        {
            //Set Deployment Key for Arction components             
            //string deploymentKey = "lgCAABW2ij+vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD+BCRGnn7c6dwaDiJovCk5g5nFwvJ+G60VSdCrAJ+jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq/B0dVcthh7ezOUzf1uXfOcEJ377/4rwUTR0VbNTCK601EN6/ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM+Q5vztCEz5k+Luaxs+S+OQD3ELg8+y7a/Dv0OhSQkqMDrR/o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry/tAsPPY26Ff3PDl1ItpFWZCzNS/xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi+VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq+F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9+B0YtxFPNBQs=";
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi-bindable chart, if you use it 
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);

            InitializeComponent();

            spControl.CursorOnFreq += spControl_CursorOnFreq;
            spControl.RangeChangeEvent += spControl_RangeChangeEvent;

            bControl.CursorOnFreq += bControl_CursorOnFreq;
            bControl.RangeChangeEvent += bControl_RangeChangeEvent;

            iControl.CursorOnFreq += iControl_CursorOnFreq;
            iControl.RangeChangeEvent += iControl_RangeChangeEvent;

            spControl.AreaFreqOnBearing += SpControl_AreaFreqOnBearing;
            spControl.AreaFreqOnQBearing += SpControl_AreaFreqOnQBearing;

            //Работа с NbarControl + SPControl
            nBar.ClickOnLeft += NBar_ClickOnLeft;
            nBar.ClickOnRight += NBar_ClickOnRight;
            nBar.Ready += NBar_Ready;

            spControl.nBarRangeMin += SpControl_nBarRangeMin;
            spControl.nBarBandWidthMHz += SpControl_nBarBandWidthMHz;
            spControl.nBarNumberOfBands += SpControl_nBarNumberOfBands;

            spControl.SendIndexesToDrawActiveRectangleOnNbar += SpControl_SendIndexesToDrawActiveRectangleOnNbar;

            spControl.ThresholdChange += SpControl_ThresholdChange;

            spControl.ExBearingChange += SpControl_ExBearingChange;
            spControl.PolarChange += SpControl_PolarChange;   

            spControl.FreqOnTargetEvent += SpControl_FreqOnTargetEvent;
            spControl.FreqOnRSEvent += SpControl_FreqOnRSEvent;
            spControl.BandOnRSEvent += SpControl_BandOnRSEvent;
            spControl.FreqOnRSSEvent += SpControl_FreqOnRSSEvent;

            spControl.OnFreqArea += SpControl_OnFreqArea;

            spControl.StorageChange += SpControl_StorageChange;
            spControl.SaveStorageChange += SpControl_SaveStorageChange;

            View1();

            _RecalcGlobalRangeMax();
            InitLettersMHz();
        }


        public delegate void OneIntEventHandler(object sender, int ChangeView);
        public event OneIntEventHandler OnIsNeedChangeView;


        public enum RDiviceType
        {
            Targetting,
            RadioSuppression,
            ControlRadioReceiver,
            ControlRadioReceiver2
        }

        private int _LettersCount = 10;
        public int LettersCount
        {
            get { return _LettersCount; }
            set
            {
                if (value < 1) value = 1;
                if (value > 10) value = 10;
                if (_LettersCount != value)
                {
                    _LettersCount = value;
                    SetCheckLetter(value);
                }
            }
        }

        List<Letter> LettersList = new List<Letter>();
        Letter CheckLetter = new Letter() { FreqMinMHz = 30, FreqMaxMHz = 6000 };

        private void InitLettersMHz()
        {
            LettersList.Clear();

            Letter letter1 = new Letter()
            {
                FreqMinMHz = 30,
                FreqMaxMHz = 50,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера I"
            };
            Letter letter2 = new Letter()
            {
                FreqMinMHz = 50,
                FreqMaxMHz = 90,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера II"
            };
            Letter letter3 = new Letter()
            {
                FreqMinMHz = 90,
                FreqMaxMHz = 160,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера III"
            };
            Letter letter4 = new Letter()
            {
                FreqMinMHz = 160,
                FreqMaxMHz = 290,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера IV"
            };
            Letter letter5 = new Letter()
            {
                FreqMinMHz = 290,
                FreqMaxMHz = 512,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера V"
            };
            Letter letter6 = new Letter()
            {
                FreqMinMHz = 512,
                FreqMaxMHz = 860,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера VI"
            };
            Letter letter7 = new Letter()
            {
                FreqMinMHz = 860,
                FreqMaxMHz = 1215,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера VII"
            };
            Letter letter8 = new Letter()
            {
                FreqMinMHz = 1215,
                FreqMaxMHz = 2000,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера VIII"
            };
            Letter letter9 = new Letter()
            {
                FreqMinMHz = 2000,
                FreqMaxMHz = 3000,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера IX"
            };
            Letter letter10 = new Letter()
            {
                FreqMinMHz = 3000,
                FreqMaxMHz = 6000,
                AngleMin = 0,
                AngleMax = 360,
                Note = "Литера X"
            };

            LettersList.Add(letter1);
            LettersList.Add(letter2);
            LettersList.Add(letter3);
            LettersList.Add(letter4);
            LettersList.Add(letter5);
            LettersList.Add(letter6);
            LettersList.Add(letter7);
            LettersList.Add(letter8);
            LettersList.Add(letter9);
            LettersList.Add(letter10);
        }

        private void SetCheckLetter(int count)
        {
            CheckLetter.FreqMinMHz = LettersList[0].FreqMinMHz;
            CheckLetter.FreqMaxMHz = LettersList[count - 1].FreqMaxMHz;
        }
        private bool isCheckLetter(Letter letterCkeck, double freqMHz)
        {
            if (letterCkeck.FreqMinMHz <= freqMHz && freqMHz <= letterCkeck.FreqMaxMHz) return true;
            else return false;
        }

        public delegate void TypeAndDoubleFreqEventHandler(object sender, RDiviceType rDiviceType, double freqMHz, short threshold);
        public event TypeAndDoubleFreqEventHandler FreqOnTypeDivece;

        private void SpControl_FreqOnTargetEvent(double freqMHz, short threshold)
        {
            if (isCheckLetter(CheckLetter, freqMHz))
                FreqOnTypeDivece?.Invoke(this, RDiviceType.Targetting, freqMHz, threshold);
        }

        private void SpControl_FreqOnRSEvent(double freqMHz, short threshold)
        {
            if (isCheckLetter(CheckLetter, freqMHz))
                FreqOnTypeDivece?.Invoke(this, RDiviceType.RadioSuppression, freqMHz, threshold);
        }

        public delegate void BandOnRSEventEventHandler(object sender, double freqLeftMHz, double freqRightMHz);
        public event BandOnRSEventEventHandler BandOnRS;

        private void SpControl_BandOnRSEvent(double freqLeftMHz, double freqRightMHz)
        {
            if (isCheckLetter(CheckLetter, freqLeftMHz) && isCheckLetter(CheckLetter, freqRightMHz))
                BandOnRS?.Invoke(this, freqLeftMHz, freqRightMHz);
        }

        private void SpControl_FreqOnRSSEvent(double freqMHz, short threshold, SpectrumPanoramaControl.SPControl.CRR crrx)
        {
            switch (crrx)
            {
                case SpectrumPanoramaControl.SPControl.CRR.CRRX:
                    FreqOnTypeDivece?.Invoke(this, RDiviceType.ControlRadioReceiver, freqMHz, threshold);
                    break;
                case SpectrumPanoramaControl.SPControl.CRR.CRRX2:
                    FreqOnTypeDivece?.Invoke(this, RDiviceType.ControlRadioReceiver2, freqMHz, threshold);
                    break;
            }
        }

        public enum FrequencyType
        {
            Forbidden = 0,
            Known = 1,
            Important = 2,
        }

        public delegate void FreqTypeAndTwoDoubleEventHandler(object sender, FrequencyType frequencyType, double startFreq, double endFreq);
        public event FreqTypeAndTwoDoubleEventHandler OnFreqArea;

        private void SpControl_OnFreqArea(SpectrumPanoramaControl.SPControl.FrequencyType frequencyType, double startFreq, double endFreq)
        {
            switch (frequencyType)
            {
                case SpectrumPanoramaControl.SPControl.FrequencyType.Forbidden:
                    OnFreqArea?.Invoke(this, FrequencyType.Forbidden, startFreq, endFreq);
                    break;
                case SpectrumPanoramaControl.SPControl.FrequencyType.Important:
                    OnFreqArea?.Invoke(this, FrequencyType.Important, startFreq, endFreq);
                    break;
                case SpectrumPanoramaControl.SPControl.FrequencyType.Known:
                    OnFreqArea?.Invoke(this, FrequencyType.Known, startFreq, endFreq);
                    break;
            }
        }

        private void SpControl_SendIndexesToDrawActiveRectangleOnNbar(object sender, SpectrumPanoramaControl.SPControl.DoubleIntEventArgs e)
        {
            nBar.DrawActiveRectangle(e.StartIndex, e.EndIndex);
        }

        private void SpControl_nBarRangeMin(double RangeMin)
        {
            nBar.RangeMin = RangeMin;
        }
        private void SpControl_nBarBandWidthMHz(double BandWidthMHz)
        {
            nBar.BandWidthMHz = BandWidthMHz;
        }
        private void SpControl_nBarNumberOfBands(int NumberOfBands)
        {
            nBar.NumberOfBands = NumberOfBands;
        }


        public delegate void Event(object sender, int index);
        public event Event NbarIndex;

        private void NBar_ClickOnLeft(object sender, NBarControl.NBar.SimpleIntEventArgs e)
        {
            spControl.NBar_ClickOnLeft(e.Index);
            NbarIndex?.Invoke(this, e.Index);

            //outRangeFromIndex(out double startValue, out double endValue, e.Index);
            //xAxis.SetRange(startValue, endValue);
        }

        private void NBar_ClickOnRight(object sender, NBarControl.NBar.DoubleIntEventArgs e)
        {
            spControl.NBar_ClickOnRight(e.StartIndex, e.EndIndex);

            //outRangeFromIndex(out double startValue, out double endValue, e.StartIndex, e.EndIndex);
            //xAxis.SetRange(startValue, endValue);
        }

        private void NBar_Ready(object sender, EventArgs e)
        {
            spControl.NBar_Ready();

            //outBandPictureBoxIndex(_GlobalRangeXmin, _GlobalRangeXmax, out int startIndex, out int endIndex);
            //nBar.DrawActiveRectangle(startIndex, endIndex);
        }

        public void CenterFreq(double FreqMHz, double BandWidthMHz)
        {
            spControl.CenterFreq(FreqMHz, BandWidthMHz);
        }

        private void UpdateRegime(int Regime)
        {
            Console.WriteLine("Panorama: Regime=" + Regime);
            switch (Regime)
            {
                case 0:
                    Stop();
                    ClearArrowsAndLines();
                    FHSSonRSVisible(false);
                    accumulationOff();
                    StorageadndBearingEnabled(true);
                    break;
                case 1:

                    isSpectrumRequest = true;
                    isScanSpeedRequest = true;
                    isAdaptiveThresholdRequest = true;

                    allowBearing = false;
                    allowExecutive = true;
                    allowIntesity = true;
                    TaskManager();
                    ClearArrowsAndLines();
                    FHSSonRSVisible(false);
                    HideSpectrumAndAdaptive(true);
                    StorageadndBearingEnabled(true);
                    break;
                case 2:

                    isSpectrumRequest = true;
                    isScanSpeedRequest = true;
                    isAdaptiveThresholdRequest = true;

                    allowBearing = true;
                    allowExecutive = true;
                    allowIntesity = true;
                    TaskManager();
                    ClearArrowsAndLines();
                    FHSSonRSVisible(false);
                    HideSpectrumAndAdaptive(true);
                    StorageadndBearingEnabled(true);
                    break;
                case 3:
                case 4:
                    ClearArrowsAndLines();

                    isSpectrumRequest = true;
                    isScanSpeedRequest = true;
                    isAdaptiveThresholdRequest = true;

                    allowBearing = false;
                    allowExecutive = false;
                    allowIntesity = true;
                    TaskManager();
                    FHSSonRSVisible(false);
                    HideSpectrumAndAdaptive(true);
                    accumulationOff();
                    StorageadndBearingEnabled(false);
                    break;
                case 5:

                    ClearArrowsAndLines();

                    isSpectrumRequest = false;
                    isScanSpeedRequest = false;
                    isAdaptiveThresholdRequest = false;

                    allowBearing = false;
                    allowExecutive = false;
                    allowIntesity = false;
                    TaskManager();
                    FHSSonRSVisible(false);
                    HideSpectrumAndAdaptive(true);
                    accumulationOff();
                    StorageadndBearingEnabled(false);
                    break;
                case 6:
                    Stop();
                    ClearArrowsAndLines();
                    FHSSonRSVisible(true);
                    HideSpectrumAndAdaptive(false);
                    accumulationOff();
                    StorageadndBearingEnabled(false);
                    break;
                default:
                    Stop();
                    ClearArrowsAndLines();
                    FHSSonRSVisible(false);
                    accumulationOff();
                    StorageadndBearingEnabled(true);
                    break;
            }
            DispatchIfNecessary(() =>
            {
                UpdateRanges(_Mode);
            });
        }

        bool RunSpectrumYet = false;
        bool RunBearingYet = false;
        bool RunPolarBearingYet = false;
        bool RunIntensityYet = false;
        bool RunExBearingYet = false;

        bool RunScanSpeedYet = false;
        bool RunAdaptiveThresholdYet = false;

        private void TaskManager()
        {
            if (_Mode != 0 || _Mode != 6 || _Mode != 7)
            {
                if (RunSpectrumYet == false)
                {
                    RunSpectrumYet = true;
                    Task.Run(() => SpectrumRequest2());
                }

                if (RunScanSpeedYet == false)
                {
                    if (FlagScanSpeed)
                    {
                        RunScanSpeedYet = true;
                        spControl.ScanSpeedVisible = true;
                        Task.Run(() => ScanSpeedRequest());
                    }
                }

                if (RunAdaptiveThresholdYet == false)
                {
                    RunAdaptiveThresholdYet = true;
                    Task.Run(() => AdaptiveThresholdRequest());
                }

                if ((_ViewMode == 2 || _ViewMode == 3) && allowBearing)
                {
                    if (RunBearingYet == false)
                    {
                        RunBearingYet = true;
                        isBearingRequest = true;
                        Task.Run(() => BearingRequest());
                    }
                }

                if (_ViewMode == 1 && RunBearingYet)
                {
                    RunBearingYet = false;
                    isBearingRequest = false;
                }

                if (_ViewMode == 4 && RunBearingYet)
                {
                    RunBearingYet = false;
                    isBearingRequest = false;
                }

                if (_ViewMode == 5 && RunBearingYet)
                {
                    RunBearingYet = false;
                    isBearingRequest = false;
                }

                if (_ViewMode == 4 && RunIntensityYet)
                {
                    RunIntensityYet = false;
                    isIntensityRequest = false;
                }

                //if (_ViewMode == 3 && allowBearing)
                //{
                //    if (RunPolarBearingYet == false)
                //    {
                //        RunPolarBearingYet = true;
                //        isPolarBearingRequest = true;
                //        Task.Run(() => PolarBearingRequest());
                //    }
                //}

                if (_ViewMode == 5 && allowIntesity)
                {
                    if (RunIntensityYet == false)
                    {
                        RunIntensityYet = true;
                        isIntensityRequest = true;
                        //Task.Run(() => IntensityRequest());
                        Task.Run(() => IntensityRequest2());
                    }
                }
            }
           
        }


        private bool allowBearing = false;
        private bool allowExecutive = false;
        private bool allowIntesity = false;

        private void Stop()
        {
            allowBearing = false;
            allowExecutive = false;
            allowIntesity = false;

            isSpectrumRequest = false;
            RunScanSpeedYet = false;

            isAdaptiveThresholdRequest = false;
            RunAdaptiveThresholdYet = false;

            isBearingRequest = false;
            isPolarBearingRequest = false;
            isIntensityRequest = false;

            RunSpectrumYet = false;
            RunBearingYet = false;
            RunPolarBearingYet = false;
            RunIntensityYet = false;
            RunExBearingYet = false;

            isScanSpeedRequest = false;
            spControl.ScanSpeedVisible = false;

            isIntensityRequest = false;

            isExBearingRequest = false;
            spControl.ExBearingOff();
        }


        private double _Decay = 0.85;
        public double Decay
        {
            get { return _Decay; }
            set
            {
                if (_Decay != value)
                {
                    _Decay = value;
                    spControl.Decay = value;
                }
            }
        }

        private bool storage = false;
        private void SpControl_StorageChange(bool value)
        {
            storage = value;
            if (value == false)
            {
                DispatchIfNecessary(() =>
                {
                    spControl.ClearStorage();
                });
            }
        }

        private bool savestorage = false;
        private void SpControl_SaveStorageChange(bool value)
        {
            savestorage = value;
            //if (value == false)
            //{
            //    DispatchIfNecessary(() =>
            //    {
            //        spControl.ClearPersistentLayerV3();
            //    });
            //}
        }

        public void IQSpectrumPainted(double requestMinFrequency, double requestMaxFrequency, byte[] Spectrum)
        {
            if (Spectrum != null)
            {
                double[] data = new double[Spectrum.Count()];
                for (int i = 0; i < Spectrum.Count(); i++)
                {
                    //int tint = Spectrum[i];
                    //data[i] = MinSpectrValue + tint;
                    data[i] = MinSpectrValue + Spectrum[i];
                }
                SpectrumPainted(requestMinFrequency, requestMaxFrequency, data);
                if (storage == true)
                    PlotStorage(requestMinFrequency, requestMaxFrequency, data);
                if (savestorage == true)
                    PlotSaveStorage(requestMinFrequency, requestMaxFrequency, data);
            }
            Cancel();
        }

        private void SpectrumPainted(double requestMinFrequency, double requestMaxFrequency, byte[] Spectrum)
        {
            DispatchIfNecessary(() =>
            {
                spControl.PlotSpectr(requestMinFrequency, requestMaxFrequency, Spectrum.Select(x => Convert.ToInt32(x)).ToArray());
            });
        }
        private void SpectrumPainted(double requestMinFrequency, double requestMaxFrequency, double[] Spectrum)
        {
            DispatchIfNecessary(() =>
            {
                spControl.PlotSpectr(requestMinFrequency, requestMaxFrequency, Spectrum);
            });
        }

        public void IQBearingPainted(int[] xData, int[] yData)
        {
            if (xData != null && yData != null)
            {
                int Count = Math.Min(xData.Count(), yData.Count());

                double[] dxData = new double[Count];
                double[] dyData = new double[Count];

                for (int i = 0; i < Count; i++)
                {
                    dxData[i] = xData[i] / 10000.0;
                    dyData[i] = yData[i] / 10.0;
                }
                IQBearingPainted(dxData, dyData);
            }
        }

        public void IQBearingPainted(double[] xData, double[] yData)
        {
            if (_Mode == 2)
            {
                if (_ViewMode == 2)
                {
                    BearingPainted(xData, yData);
                }
                if (_ViewMode == 3)
                {
                    BearingPainted(xData, yData);
                    PolarBearingPainted(xData, yData);
                }
            }
        }

        private void BearingPainted(double[] xData, double[] yData)
        {
            DispatchIfNecessary(() =>
            {
                bControl.BearingPainted(xData, yData);
            });
        }
        private void PolarBearingPainted(double[] xData, double[] yData)
        {
            DispatchIfNecessary(() =>
            {
                pbControl.BearingPainted(xData, yData);
            });
        }

        public void IQExBearingPaited(short Direction, byte[] CorrelationHistogram, int Frequency, short StandardDeviation, short DiscardedDirectionPercent)
        {
            double angle = Direction / 10.0;
            double[] data = new double[CorrelationHistogram.Length];
            for (int i = 0; i < CorrelationHistogram.Length; i++)
            {
                data[i] = CorrelationHistogram[i];
            }
            PolarControl(angle, data);

            double freq = Frequency / 10000.0;
            double sd = StandardDeviation / 10.0;
            double percent = DiscardedDirectionPercent;
            TextControl(freq, angle, sd, percent);
        }

        public void QBearingPaited(int Frequency, short Direction, short Direction2, short StandardDeviation, short DiscardedDirectionPercent)
        {
            if (_Mode == 1 || _Mode == 2)
            {
                tControl.QPanelV = true;
                if (_ViewMode != 4)
                {
                    ViewMode = 4;
                    OnIsNeedChangeView?.Invoke(this, ViewMode);
                }

                double freq = Frequency / 10000.0;
                double angle = Direction / 10.0;
                double angle2 = -1;
                if (Direction2 != -1)
                    angle2 = Direction2 / 10.0;
                double sd = StandardDeviation / 10.0;
                double percent = DiscardedDirectionPercent;

                PolarControl(angle);

                TextControl(freq, angle, angle2, sd, percent);
            }
        }

        public void PolarControl(double angle)
        {
            DispatchIfNecessary(() =>
            {
                pControl.InitAngle2(angle);
                pControl.ClearData();
            });
        }
        public void PolarControl(double angle, double[] data)
        {
            DispatchIfNecessary(() =>
            {
                pControl.InitAngle2(angle);
                pControl.PlotCorrelationCurve2(data);
            });
        }
        public void TextControl(double freq, double avbearing, double sd, double percent)
        {
            DispatchIfNecessary(() =>
            {
                tControl.Frequency = freq;
                tControl.Bearing = avbearing;
                tControl.SD = sd;
                tControl.LeavePercent = percent;
            });
        }
        public void TextControl(double freq, double avbearing, double avbearing2, double sd, double percent)
        {
            DispatchIfNecessary(() =>
            {
                tControl.Frequency = freq;
                tControl.Bearing = avbearing;
                tControl.Bearing2 = avbearing2;
                tControl.SD = sd;
                tControl.LeavePercent = percent;
            });
        }

        public void IQIntensityPaint(byte[] data, double MinFrequency, double MaxFrequency, int NPointCount, byte TimeLength)
        {
            double[,] dd = new double[NPointCount, TimeLength];

            //прямой порядок
            int k = 0;
            int count = 0;
            for (int i = 0; i < data.Count(); i++)
            {
                //double temp = (-1) * (double)data.Spectrum[i];
                double temp = MinSpectrValue + data[i];
                //if (temp < porogCursor.YPosition || temp == 0)
                //    temp = -121;
                dd[i - (NPointCount * k), k] = temp;
                count++;
                if (count == NPointCount)
                {
                    count = 0;
                    k++;
                }
            }

            IntensityPaint(MinFrequency, MaxFrequency, dd);

            CancelIntensity();
        }

        public void IntensityPaint(double xStart, double xEnd, double[,] data)
        {
            DispatchIfNecessary(() =>
            {
                iControl.PlotData(xStart, xEnd, data);
            });
        }

        private void iControl_RangeChangeEvent(double startFreq, double endFreq)
        {
            spControl.ChangeXRange(startFreq, endFreq);
        }

        private void bControl_RangeChangeEvent(double startFreq, double endFreq)
        {
            spControl.ChangeXRange(startFreq, endFreq);
        }

        private void spControl_RangeChangeEvent(double startFreq, double endFreq)
        {
            bControl.ChangeXRange(startFreq, endFreq);

            iControl.ChangeXRange(startFreq, endFreq);
        }

        private void bControl_CursorOnFreq(double freq)
        {
            spControl.cursorX = freq;
            spControl.UpdateCursorLabel();
            iControl.cursorX = freq;
        }

        private void iControl_CursorOnFreq(double freq)
        {
            spControl.cursorX = freq;
            spControl.UpdateCursorLabel();
            bControl.cursorX = freq;
        }

        private void spControl_CursorOnFreq(double freq)
        {
            bControl.cursorX = freq;
            iControl.cursorX = freq;
        }


        public void IQDrawRSArrows(bool isActive, Color[] color, int[] Frequencies, int[] Amplitudes)
        {
            if (_Mode == 3 || _Mode == 4 || _Mode == 5)
            {
                if (Frequencies != null && Amplitudes != null)
                {
                    double[] xData = new double[Frequencies.Count()];
                    double[] yData = new double[Amplitudes.Count()];
                    for (int i = 0; i < xData.Count(); i++)
                    {
                        xData[i] = Frequencies[i] / 10000d;
                        yData[i] = (Amplitudes[i] <= 0) ? Amplitudes[i] : Amplitudes[i] * -1d;
                    }

                    DispatchIfNecessary(() =>
                    {
                        //spControl.ClearArrowsAndLines();
                        spControl.DrawRSArrows(color, xData, yData);
                    });

                    double[] Values = xData;
                    int[] indexes = ConvertValueToIndexes(Values);
                    indexes = indexes.Distinct().ToArray();

                    DispatchIfNecessary(() =>
                    {
                        if (isActive == true) nBar.DrawActiveRSCircles(indexes);
                        else nBar.DrawNonActiveRSCircles(indexes);
                    });
                }
            }
        }

        public void DrawRSArrows(bool isActive, double[] xData, double[] yData)
        {
            DispatchIfNecessary(() =>
            {
                spControl.DrawRSArrows(isActive, xData, yData);
            });
        }

        public void DrawRSLines(double[] xData, double yLevel)
        {
            DispatchIfNecessary(() =>
            {
                spControl.ClearArrowsAndLines();
                spControl.DrawRSLines(xData, yLevel);
            });
        }

        private bool _FHSSTest = true;
        public bool FHSSTest
        {
            get { return _FHSSTest; }
            set
            {
                if (_FHSSTest != value)
                {
                    _FHSSTest = value;
                }
            }
        }

        public void IQDrawRSLines(int[] Frequencies, int yLevel = -80)
        {
            if (Frequencies != null)
            {
                double[] xData = new double[Frequencies.Count()];
                for (int i = 0; i < xData.Count(); i++)
                {
                    xData[i] = Frequencies[i] / 10000d;
                }

                if (FHSSTest)
                {
                    DispatchIfNecessary(() =>
                    {
                        spControl.ClearArrowsAndLines();
                        spControl.DrawRSLines(xData, yLevel);
                    });
                }
                else
                {
                    DispatchIfNecessary(() =>
                    {
                        spControl.ClearArrowsAndLines2();
                        spControl.DrawRSLines2(xData, yLevel);
                    });
                }

                double[] Values = xData;
                int[] indexes = ConvertValueToIndexes(Values);
                indexes = indexes.Distinct().ToArray();

                DispatchIfNecessary(() =>
                {
                    nBar.DrawActiveRSCircles(indexes);
                });

            }
        }

        public void IQDrawRSLines(List<double[]> lxDataMHz, List<double> lyLevelnegative)
        {
            DispatchIfNecessary(() =>
            {
                spControl.ClearArrowsAndLines();
                spControl.DrawRSLines(lxDataMHz, lyLevelnegative);
            });
        }

        public void ClearArrowsAndLines()
        {
            DispatchIfNecessary(() =>
            {
                spControl.ClearArrowsAndLines();
            });
            DispatchIfNecessary(() =>
            {
                nBar.ClearRSCircles();
            });
        }

        private void FHSSonRSVisible(bool visible)
        {
            DispatchIfNecessary(() =>
            {
                spControl.FHSSonRSVisible = visible;
            });
        }

        private void PlotStorage(double startFreq, double endFreq, double[] dataArray)
        {
            DispatchIfNecessary(() =>
            {
                //spControl.PlotStorage(startFreq, endFreq, dataArray);
                //spControl.PlotStorageV1(startFreq, endFreq, dataArray); //18%

                //spControl.PlotStorageV2(startFreq, endFreq, dataArray);

                //spControl.PlotStorageV4(startFreq, endFreq, dataArray); //29%
                spControl.PlotStorageV5(startFreq, endFreq, dataArray); //23%

            });
        }

        public void ClearStorage()
        {
            DispatchIfNecessary(() =>
            {
                spControl.ClearStorage();
            });
        }

        public void ExternalExBearing(double freqMHz, double freqWidthMHz)
        {
            DispatchIfNecessary(() =>
            {
                spControl.ExternalExBearing(freqMHz, freqWidthMHz);
            });
        }

        private void PlotSaveStorage(double startFreq, double endFreq, double[] dataArray)
        {
            DispatchIfNecessary(() =>
            {
                spControl.PlotSaveStorage(startFreq, endFreq, dataArray); 
            });
        }

        private void HideSpectrumAndAdaptive(bool value)
        {
            spControl.HideSpectrumAndAdaptive(value);
        }

        private void accumulationOff()
        {
            spControl.accumulationOff();
        }

        private void StorageadndBearingEnabled(bool value)
        {
            spControl.StorageadndBearingEnabled(value);
        }

        public void qBearingButtonEnabled(bool value)
        {
            spControl.qBearingButtonEnabled(value);
        }

        public void FHSSonRS(List<double> lFreqStartMHz, List<double> lFreqEndMHz, List<double[]> lCutOffFreqMHz, List<double[]> lCutOffWidthMHz, List<double> lNegativeThreshold)
        {
            DispatchIfNecessary(() =>
            {
                spControl.ClearFHSSonRS();
                spControl.FHSSonRS(lFreqStartMHz, lFreqEndMHz, lCutOffFreqMHz, lCutOffWidthMHz, lNegativeThreshold);
            });
        }

        public void FillRPRectangles(int[] indexes)
        {
            DispatchIfNecessary(() =>
            {
                nBar.FillRPRectangles(indexes);
            });
        }

        public void FillRSRectangles(int[] indexes)
        {
            DispatchIfNecessary(() =>
            {
                nBar.FillRSRectangles(indexes);
            });
        }

        public void DrawActiveRSCircles(int[] indexes)
        {
            DispatchIfNecessary(() =>
            {
                nBar.DrawActiveRSCircles(indexes);
            });
        }

        public void DrawNonActiveRSCircles(int[] indexes)
        {
            DispatchIfNecessary(() =>
            {
                nBar.DrawNonActiveRSCircles(indexes);
            });
        }

        private void outRangeFromIndex(out double startValue, out double endValue, int index)
        {
            startValue = _GlobalRangeXmin + _GlobalBandWidthMHz * index;
            endValue = startValue + _GlobalBandWidthMHz;
        }
        private void outRangeFromIndex(out double startValue, out double endValue, int startIndex, int endIndex)
        {
            startValue = _GlobalRangeXmin + _GlobalBandWidthMHz * startIndex;
            endValue = _GlobalRangeXmin + _GlobalBandWidthMHz * (endIndex + 1);
        }

        public void GetStartAndEndIndexesFromRange(out int startIndex, out int endIndex, double startValue, double endValue)
        {
            double[] Values = { startValue, endValue };
            var intarr = ConvertValueToIndexes(Values);
            startIndex = intarr[0];
            endIndex = intarr[1];
        }

        private List<double> Divide = new List<double>();

        private void RecalcDivide()
        {
            Divide.Clear();
            for (int i = 1; i < _GlobalNumberOfBands + 1; i++)
            {
                Divide.Add(_GlobalRangeXmin + _GlobalBandWidthMHz * i);
            }
            UpdateRanges(_Mode);
        }

        private int _RSRectanglesMode = 0;
        public int RSRectanglesMode
        {
            get { return _RSRectanglesMode; }
            set
            {
                if (_RSRectanglesMode != value)
                {
                    _RSRectanglesMode = value;
                    UpdateRanges(_Mode);
                }
            }
        }

        private double[] MinFreqsRI;
        private double[] MaxFreqsRI;

        private double[] MinFreqsRS;
        private double[] MaxFreqsRS;

        private double[] FreqsRS;

        public enum FreqsType
        {
            FreqsRI,
            FreqsRS
        }

        public void ImportFreqs(FreqsType freqsType, double[] MinFreqs, double[] MaxFreqs)
        {
            switch (freqsType)
            {
                case FreqsType.FreqsRI:
                    ImportFreqs(ref MinFreqsRI, ref MaxFreqsRI, MinFreqs, MaxFreqs);
                    break;
                case FreqsType.FreqsRS:
                    ImportFreqs(ref MinFreqsRS, ref MaxFreqsRS, MinFreqs, MaxFreqs);
                    break;
            }
            UpdateRanges(_Mode);
        }

        public void ImportParFreqsRS(double[] Freqs)
        {
            FreqsRS = Freqs;
            UpdateRanges(_Mode);
        }

        private void ImportFreqs(ref double[] TargetMinFreqs, ref double[] TargetMaxFreqs, double[] MinFreqs, double[] MaxFreqs)
        {
            if (MinFreqs != null && MaxFreqs != null)
            {
                int Count = Math.Min(MinFreqs.Count(), MaxFreqs.Count());
                TargetMinFreqs = new double[Count];
                TargetMaxFreqs = new double[Count];
                for (int i = 0; i < Count; i++)
                {
                    TargetMinFreqs[i] = MinFreqs[i];
                    TargetMaxFreqs[i] = MaxFreqs[i];
                }
            }
        }

        private (int indexStart, int indexEnd) EjectIndex(double MinFreq, double MaxFreq)
        {
            var w = ConvertValueToIndexes(new double[] { MinFreq, MaxFreq });
            if (w.Count() == 2)
                return (w[0], w[1]);
            else return (-1, -1);
        }

        public (int indexStart, int indexEnd) CalcLikePro(double StartFrequency, double EndFrequency)
        {
            int[] checkArr = new int[_GlobalNumberOfBands + 1];

            for (int i = 0; i < _GlobalNumberOfBands + 1; i++)
            {
                checkArr[i] = (int)(_GlobalRangeXmin + i * GlobalBandWidthMHz);
            }

            int startIndex = -1;
            int endIndex = -1;

            for (int w = 0; w < _GlobalNumberOfBands; w++)
            {
                if (StartFrequency >= checkArr[w] && StartFrequency < checkArr[w + 1])
                {
                    startIndex = w;
                    break;
                }
            }
            for (int w = (startIndex == 0) ? 0 : startIndex - 1; w < _GlobalNumberOfBands; w++)
            {
                if (EndFrequency > checkArr[w] && EndFrequency <= checkArr[w + 1])
                {
                    endIndex = w;
                    break;
                }
            }
            return (startIndex, endIndex);
        }

        public int[] ConvertValueToIndexes(double[] Values)
        {
            List<int> lint = new List<int>();

            for (int i = 0; i < Values.Count(); i++)
            {
                if (Values[i] >= _GlobalRangeXmin && Values[i] <= _GlobalRangeXmax)
                {
                    int index = (int)((Values[i] - _GlobalRangeXmin) / _GlobalBandWidthMHz); ;
                    if (Divide.Contains(Values[i])) index--;
                    lint.Add(index);
                }
            }
            return lint.ToArray();
        }

        private int[] GenerateFromStartToEndRow(int start, int end)
        {
            List<int> lint = new List<int>();
            for (int i = start; i < end; i++)
            {
                lint.Add(i);
            }
            return lint.ToArray();
        }

        private List<int> GenerateFromStartToEndRowList(int start, int end)
        {
            List<int> lint = new List<int>();
            for (int i = start; i <= end; i++)
            {
                lint.Add(i);
            }
            return lint;
        }

        List<(int indexStart, int indexEnd)> listTyples = new List<(int indexStart, int indexEnd)>();

        List<int> RIBandsRow = new List<int>();

        private void DoIt(FreqsType freqsType, double[] TargetMinFreqs, double[] TargetMaxFreqs)
        {
            if (TargetMinFreqs != null && TargetMaxFreqs != null)
            {
                int Count = Math.Min(TargetMinFreqs.Count(), TargetMaxFreqs.Count());
                for (int i = 0; i < Count; i++)
                {
                    //var temp = EjectIndex(TargetMinFreqs[i], TargetMaxFreqs[i]);
                    var temp = CalcLikePro(TargetMinFreqs[i], TargetMaxFreqs[i]);
                    if (temp.indexStart != -1 && temp.indexEnd != -1)
                    {
                        listTyples.Add(temp);
                    }
                }

                List<int> Row = new List<int>();

                for (int i = 0; i < listTyples.Count(); i++)
                {
                    var row = GenerateFromStartToEndRowList(listTyples[i].indexStart, listTyples[i].indexEnd);
                    Row = Row.Concat(row).ToList<int>();
                }
                switch (freqsType)
                {
                    case FreqsType.FreqsRI:
                        nBar.FillRPRectangles(Row.ToArray());
                        RIBandsRow = new List<int>(Row);
                        break;
                    case FreqsType.FreqsRS:
                        nBar.FillRSRectangles(Row.ToArray());
                        break;
                }

                listTyples.Clear();
                Row.Clear();
            }
        }

        private void DoItSmaller()
        {
            nBar.FillRSRectangles(ConvertValueToIndexes(FreqsRS));
        }

        private void UpdateRanges(int Regime)
        {
            switch (Regime)
            {
                case 0:
                case 1:
                case 2:
                    DoIt(FreqsType.FreqsRI, MinFreqsRI, MaxFreqsRI);
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    if (_RSRectanglesMode == 0)
                        DoIt(FreqsType.FreqsRS, MinFreqsRS, MaxFreqsRS);
                    else
                        DoItSmaller();
                    break;
                case 7:
                    nBar.ClearRectangles();
                    break;
            }
        }

        double startExBearingFreq;
        double endExBearingFreq;
        private void SpControl_AreaFreqOnBearing(double startFreq, double endFreq)
        {
            tControl.QPanelV = false;
            startExBearingFreq = startFreq;
            endExBearingFreq = endFreq;
        }

        private void SpControl_AreaFreqOnQBearing(double startFreqMHz, double endFreqMHz)
        {
            //tControl.QPanelV = true;
            if (_Mode == 1 || _Mode == 2)
            {
                //if (_ViewMode != 4)
                //{
                //    ViewMode = 4;
                //    OnIsNeedChangeView?.Invoke(this, ViewMode);
                //}
                DispatchIfNecessary(() =>
                {
                    NeedQBearingRequest?.Invoke(this, startFreqMHz, endFreqMHz, tControl.AvPhases, tControl.AvBearings);
                });
            }
        }

        public void PaintSpecBands(FrequencyType frequencyType, double[] FreqStartMHz, double[] FreqEndMHz)
        {
            DispatchIfNecessary(() =>
            {
                spControl.PaintSpecBands((byte)frequencyType, FreqStartMHz, FreqEndMHz);
            });
        }

        public void CursorCRRChange(double FreqMHz, double BwMHz, RDiviceType rDiviceType = RDiviceType.ControlRadioReceiver)
        {
            DispatchIfNecessary(() =>
            {
                switch (rDiviceType)
                {
                    case RDiviceType.ControlRadioReceiver:
                        spControl.CursorCRRChange(FreqMHz, BwMHz, SpectrumPanoramaControl.SPControl.CRR.CRRX);
                        break;
                    case RDiviceType.ControlRadioReceiver2:
                        spControl.CursorCRRChange(FreqMHz, BwMHz, SpectrumPanoramaControl.SPControl.CRR.CRRX2);
                        break;
                    default:
                        break;
                }
            });
        }

        public void CRRXVisibleChange(RDiviceType rDiviceType, bool visible)
        {
            DispatchIfNecessary(() =>
            {
                switch (rDiviceType)
                {
                    case RDiviceType.ControlRadioReceiver:
                        spControl.CRRXVisible = visible;
                        break;
                    case RDiviceType.ControlRadioReceiver2:
                        spControl.CRRX2Visible = visible;
                        break;
                    default:
                        break;
                }
            });
        }

        private double CalcVStar()
        {
            var v1 = spControl.ActualHeight;
            var v3 = spControl.ChartAHeight;
            //Console.WriteLine(v3 / v1);
            return v3 / v1;
        }

        private double CalcPHeight(int index)
        {

            var aHeight = spControl.ActualHeight;

            aHeight = (spControl.topPanelVisible) ? aHeight - 27 : aHeight;
            aHeight = (spControl.firstPanelVisible) ? aHeight - 22 : aHeight;
            //aHeight = (spControl.bottomPanelVisible) ? aHeight - 24 : aHeight;

            aHeight = (index == 1) ? spControl.ActualHeight / 2 : spControl.ActualHeight;

            return aHeight;
        }

        private void View(int index)
        {
            switch (index)
            {
                case 1:
                    View1();
                    break;
                case 2:
                    View2();
                    break;
                case 3:
                    View3();
                    break;
                case 4:
                    View4();
                    break;
                case 5:
                    View5();
                    break;
                default:
                    View1();
                    break;
            }
        }

        private void View1()
        {
            _ViewMode = 1;
            myGrid.RowDefinitions[2].Height = new GridLength(0);
            myGrid.RowDefinitions[3].Height = new GridLength(0);
            myGrid.RowDefinitions[4].Height = new GridLength(10);
            myGrid.RowDefinitions[5].Height = new GridLength(24);
            myGrid.RowDefinitions[6].Height = new GridLength(5);

            myGrid.ColumnDefinitions[1].Width = new GridLength(0);
            myGrid.ColumnDefinitions[2].Width = new GridLength(0);

            spControl.xRangeLabelVisible = true;
            spControl.vrbMHz = Visibility.Visible;
            //spControl.xAxisLabelVisible = true;
        }

        private void View2()
        {
            _ViewMode = 2;
            myGrid.RowDefinitions[2].Height = new GridLength(0.75, GridUnitType.Star);
            myGrid.RowDefinitions[3].Height = new GridLength(0);
            myGrid.RowDefinitions[4].Height = new GridLength(10);
            myGrid.RowDefinitions[5].Height = new GridLength(24);
            myGrid.RowDefinitions[6].Height = new GridLength(5);

            myGrid.ColumnDefinitions[1].Width = new GridLength(0);
            myGrid.ColumnDefinitions[2].Width = new GridLength(0);

            spControl.xRangeLabelVisible = false;
            spControl.vrbMHz = Visibility.Hidden;
            //spControl.xAxisLabelVisible = false;
            //spControl.yAxisLabelVisible = false;

            //bControl.xRangeLabelVisible = false;
            //bControl.xAxisLabelVisible = false;
            //bControl.yAxisLabelVisible = false;
        }

        private void View3()
        {
            _ViewMode = 3;
            myGrid.RowDefinitions[2].Height = new GridLength(0.75, GridUnitType.Star);
            myGrid.RowDefinitions[3].Height = new GridLength(0);
            myGrid.RowDefinitions[4].Height = new GridLength(10);
            myGrid.RowDefinitions[5].Height = new GridLength(24);
            myGrid.RowDefinitions[6].Height = new GridLength(5);

            myGrid.ColumnDefinitions[1].Width = new GridLength(0.75, GridUnitType.Star);
            myGrid.ColumnDefinitions[2].Width = new GridLength(0, GridUnitType.Star);

            spControl.xRangeLabelVisible = false;
            spControl.vrbMHz = Visibility.Hidden;
            //spControl.xAxisLabelVisible = true;
        }

        private void View4()
        {
            _ViewMode = 4;
            myGrid.RowDefinitions[2].Height = new GridLength(0);
            myGrid.RowDefinitions[3].Height = new GridLength(0);
            myGrid.RowDefinitions[4].Height = new GridLength(10);
            myGrid.RowDefinitions[5].Height = new GridLength(24);
            myGrid.RowDefinitions[6].Height = new GridLength(5);

            myGrid.ColumnDefinitions[1].Width = new GridLength(0, GridUnitType.Star);
            myGrid.ColumnDefinitions[2].Width = new GridLength(0.75, GridUnitType.Star);
           
            spControl.xRangeLabelVisible = true;
            spControl.vrbMHz = Visibility.Visible;
            //spControl.xAxisLabelVisible = true;
        }

        private void View5()
        {
            _ViewMode = 5;
            myGrid.RowDefinitions[2].Height = new GridLength(0);
            myGrid.RowDefinitions[3].Height = new GridLength(0.75, GridUnitType.Star);
            myGrid.RowDefinitions[4].Height = new GridLength(10);
            myGrid.RowDefinitions[5].Height = new GridLength(24);
            myGrid.RowDefinitions[6].Height = new GridLength(5);

            myGrid.ColumnDefinitions[1].Width = new GridLength(0);
            myGrid.ColumnDefinitions[2].Width = new GridLength(0);

            spControl.xRangeLabelVisible = false;
            spControl.vrbMHz = Visibility.Hidden;
            //spControl.xAxisLabelVisible = false;
        }

       

        private void AFP_Click(object sender, RoutedEventArgs e)
        {
            ViewMode = 1;
        }

        private void FBP_Click(object sender, RoutedEventArgs e)
        {
            ViewMode = 2;
        }

        private void AFPandL_Click(object sender, RoutedEventArgs e)
        {
            ViewMode = 3;
        }

        private void FTP_Click(object sender, RoutedEventArgs e)
        {
            ViewMode = 4;
        }
    }

    class Letter
    {
        public double FreqMinMHz;
        public double FreqMaxMHz;
        public short AngleMin = 0;
        public short AngleMax = 360;
        public string Note = "";
    }
}
