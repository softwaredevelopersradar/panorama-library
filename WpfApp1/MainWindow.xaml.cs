﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AWPtoDSPprotocolFuture dsp = new AWPtoDSPprotocolFuture();

        public MainWindow()
        {
            InitializeComponent();

            InitDSP();

            pLibrary.ViewMode = 1;
        }

        public void InitDSP()
        {
            //dsp.ModeMessageUpdate += Dsp_ModeMessageUpdate;
            //dsp.FiltersMessageUpdate += Dsp_FiltersMessageUpdate;
            dsp.RadioJamStateUpdate += Dsp_RadioJamStateUpdate;
            dsp.FhssRadioJamUpdate += Dsp_FhssRadioJamUpdate;
            //dsp.StationLocationMessageUpdate += Dsp_StationLocationMessageUpdate;

            //dsp.IsConnected += Dsp_IsConnected;
            //dsp.IsRead += Dsp_IsRead;
            //dsp.IsWrite += Dsp_IsWrite;

            //dsp.StationLocationMessageUpdate += Dsp_StationLocationMessageUpdate1;
            //dsp.SetTimeUpdate += Dsp_SetTimeUpdate;
            //dsp.NumberOfSatellitesUpdate += Dsp_NumberOfSatellitesUpdate;

            //dsp.ShaperModeUpdate += Dsp_ShaperModeUpdate;
            //dsp.ShaperVoltageUpdate += dsp_ShaperVoltageUpdate;
            //dsp.ShaperPowerUpdate += dsp_ShaperPowerUpdate;
            //dsp.ShaperTemperatureUpdate += dsp_ShaperTemperatureUpdate;
            //dsp.ShaperAmperageUpdate += dsp_ShaperAmperageUpdate;



            pLibrary.NeedSpectrumRequest += PLibrary_NeedSpectrumRequest;

            pLibrary.NeedAdaptiveThresholdRequest += PLibrary_NeedAdaptiveThresholdRequest;

            pLibrary.NeedAdaptiveThresholdRequest2 += PLibrary_NeedAdaptiveThresholdRequest2;

            pLibrary.NeedExBearingRequest += PLibrary_NeedExBearingRequest;

            pLibrary.NeedIntensityRequest += PLibrary_NeedIntensityRequest;

            pLibrary.NeedBearingRequest += PLibrary_NeedBearingRequest;

            pLibrary.NeedIntensityRequest += PLibrary_NeedIntensityRequest;

            pLibrary.ThresholdChange += PLibrary_ThresholdChangeAsync;

        }

       

        private object lockObject = new object();

        private async void PLibrary_ThresholdChangeAsync(object sender, int value)
        {
            var answer00 = await dsp.SetFilters(value, 0, 0, 0);
        }

        private void Dsp_FhssRadioJamUpdate(FhssRadioJamUpdateEvent answer)
        {
            pLibrary.IQDrawRSLines(answer.Frequencies);
            //double[] datax = new double[answer.JammedFrequenciesCount];
            //for (int i = 0; i < datax.Count(); i++)
            //{
            //    datax[i] = answer.Frequencies[i] / 10000d;
            //}
            //pLibrary.DrawRSLines(datax, -80);
        }

        private void Dsp_RadioJamStateUpdate(RadioJamStateUpdateEvent answer)
        {
            if (answer.TargetStates[0].RadioJamState == 1)
            {
                double[] datax = new double[answer.TargetStates.Count()];
                double[] datay = new double[answer.TargetStates.Count()];
                for (int i = 0; i < answer.TargetStates.Count(); i++)
                {
                    datax[i] = answer.TargetStates[i].Frequency / 10000d;
                    datay[i] = answer.TargetStates[i].Amplitude / -1d;
                }
                pLibrary.DrawRSArrows(true, datax, datay);
            }
            else
                pLibrary.ClearArrowsAndLines();
        }

        //1
        private void one_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 1;
        }
        //2
        private void two_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 2;
        }
        //3
        private void two_plus_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 3;
        }
        //4
        private void three_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 4;
        }
        //5
        private void four_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ViewMode = 5;
        }
        //Connect/Disconnect
        private async void five_Click(object sender, RoutedEventArgs e)
        {
            SolidColorBrush brush = (SolidColorBrush)five.Background;
            if (brush.Color == Colors.Red)
            {
                var Address = "192.168.1.102";
                //var Address = "192.168.0.193";
                //var Address = "192.168.0.111";
                var Port = 10001;
                await dsp.ConnectToBearingDSP(Address, Port);

                five.Background = new SolidColorBrush(Colors.Green);
            }
            else
            {
                dsp.DisconnectFromBearingDSP();
                five.Background = new SolidColorBrush(Colors.Red);
            }
        }

        //Подготовка
        private async void six_Click(object sender, RoutedEventArgs e)
        {
            var answer = await dsp.SetMode(0);
            pLibrary.Mode = 0;

        }

        //РР 1
        private async void seven0_Click(object sender, RoutedEventArgs e)
        {
            //Установка режима работы с эфира
            var answer00 = await dsp.SetReceiversChannel(0);

            //установка курсового угла
            var answer0 = await dsp.SetDirectionCorrection(0, true);

            //установка диапазонов и секторов радиоразведки
            var answer = await dsp.SetSectorsAndRanges(0, 0, 25, 100, 0, 360); //Шифр 3

            //установка фильтов для обнаружения ИРИ
            answer = await dsp.SetFilters(-80, 0, 0, 0); //Шифр 18

            //установка режима
            answer = await dsp.SetMode(1);

            pLibrary.Mode = 1;
        }

        //РР 2
        private async void seven_Click(object sender, RoutedEventArgs e)
        {
            //Установка режима работы с эфира
            var answer00 = await dsp.SetReceiversChannel(0);

            //установка курсового угла
            var answer0 = await dsp.SetDirectionCorrection(0, true);

            //установка диапазонов и секторов радиоразведки
            var answer = await dsp.SetSectorsAndRanges(0, 0, 25, 100, 0, 360); //Шифр 3

            //установка фильтов для обнаружения ИРИ
            answer = await dsp.SetFilters(-80, 0, 0, 0); //Шифр 18

            //установка режима
            answer = await dsp.SetMode(2);

            pLibrary.Mode = 2;
        }
        
        //РП
        private async void eight_Click(object sender, RoutedEventArgs e)
        {
            if (dsp != null)
            {
                try
                {
                    //Установка режима работы с эфира
                    //var answer00 = await dsp.SetReceiversChannel(0);

                    //установка курсового угла
                    var answer0 = await dsp.SetDirectionCorrection(0, true);

                    //установка диапазонов и секторов радиоподавления
                    var answerSSaR = await dsp.SetSectorsAndRanges(1, 0, 25, 100, 0, 360); //Шифр 3
                    //panoramaControl1.SetSectorsAndRanges(variableWork.RangeSectorSupprOwn);

                    byte RegimeRadioSuppr = 0;
                    if (rb1.IsChecked.Value) RegimeRadioSuppr = 0;
                    if (rb2.IsChecked.Value) RegimeRadioSuppr = 1;
                    if (rb3.IsChecked.Value) RegimeRadioSuppr = 2;
                    if (rb4.IsChecked.Value) RegimeRadioSuppr = 3;

                    switch (RegimeRadioSuppr + 3)
                    {
                        case 3:

                            //Установка параметров радиоподавлениЯ для ФРЧ, Шифр 32
                            var answerSettingsFrs = await dsp.SetFrsRadioJamSettings(800, 10 * 1000, 5);

                            Protocols.FRSJammingSetting[] fRSJammingSettingsFrs = new Protocols.FRSJammingSetting[3];

                            {
                                fRSJammingSettingsFrs[0].DeviationCode = 0;
                                fRSJammingSettingsFrs[0].DurationCode = 0;
                                fRSJammingSettingsFrs[0].Frequency = 350000; // ?
                                fRSJammingSettingsFrs[0].Id = 0;
                                fRSJammingSettingsFrs[0].Liter = 0;
                                fRSJammingSettingsFrs[0].ManipulationCode = 0;
                                fRSJammingSettingsFrs[0].ModulationCode = 0;
                                fRSJammingSettingsFrs[0].Priority = 1;
                                fRSJammingSettingsFrs[0].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsFrs[1].DeviationCode = 1;
                                fRSJammingSettingsFrs[1].DurationCode = 1;
                                fRSJammingSettingsFrs[1].Frequency = 400000; // ?
                                fRSJammingSettingsFrs[1].Id = 1;
                                fRSJammingSettingsFrs[1].Liter = 0;
                                fRSJammingSettingsFrs[1].ManipulationCode = 1;
                                fRSJammingSettingsFrs[1].ModulationCode = 1;
                                fRSJammingSettingsFrs[1].Priority = 1;
                                fRSJammingSettingsFrs[1].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsFrs[2].DeviationCode = 2;
                                fRSJammingSettingsFrs[2].DurationCode = 2;
                                fRSJammingSettingsFrs[2].Frequency = 450000; // ?
                                fRSJammingSettingsFrs[2].Id = 2;
                                fRSJammingSettingsFrs[2].Liter = 0;
                                fRSJammingSettingsFrs[2].ManipulationCode = 2;
                                fRSJammingSettingsFrs[2].ModulationCode = 2;
                                fRSJammingSettingsFrs[2].Priority = 1;
                                fRSJammingSettingsFrs[2].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsFrs[2].DeviationCode = 2;
                                fRSJammingSettingsFrs[2].DurationCode = 2;
                                fRSJammingSettingsFrs[2].Frequency = 900000; // ?
                                fRSJammingSettingsFrs[2].Id = 2;
                                fRSJammingSettingsFrs[2].Liter = 0;
                                fRSJammingSettingsFrs[2].ManipulationCode = 2;
                                fRSJammingSettingsFrs[2].ModulationCode = 2;
                                fRSJammingSettingsFrs[2].Priority = 1;
                                fRSJammingSettingsFrs[2].Threshold = 130;
                            }


                            //Установка ИРИ ФРЧ для РП, Шифр 6
                            var answerFreqFrs = await dsp.SetFRSJamming((byte)0, fRSJammingSettingsFrs);
                            break;

                        case 4:

                            //Установка параметров радиоподавлениЯ для АПРЧ, Шифр 55
                            var answerSettings = await dsp.SetAfrsRadioJamSettings(
                                800,
                                10 * 1000,
                                5,
                               10 * 10000,
                                (short)(100 * 10),
                               -90);

                            Protocols.FRSJammingSetting[] fRSJammingSettingsAfrs = new Protocols.FRSJammingSetting[3];
                            {
                                fRSJammingSettingsAfrs[0].DeviationCode = 0;
                                fRSJammingSettingsAfrs[0].DurationCode = 0;
                                fRSJammingSettingsAfrs[0].Frequency = 350000; // ?
                                fRSJammingSettingsAfrs[0].Id = 0;
                                fRSJammingSettingsAfrs[0].Liter = 0;
                                fRSJammingSettingsAfrs[0].ManipulationCode = 0;
                                fRSJammingSettingsAfrs[0].ModulationCode = 0;
                                fRSJammingSettingsAfrs[0].Priority = 1;
                                fRSJammingSettingsAfrs[0].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsAfrs[1].DeviationCode = 1;
                                fRSJammingSettingsAfrs[1].DurationCode = 1;
                                fRSJammingSettingsAfrs[1].Frequency = 400000; // ?
                                fRSJammingSettingsAfrs[1].Id = 1;
                                fRSJammingSettingsAfrs[1].Liter = 0;
                                fRSJammingSettingsAfrs[1].ManipulationCode = 1;
                                fRSJammingSettingsAfrs[1].ModulationCode = 1;
                                fRSJammingSettingsAfrs[1].Priority = 1;
                                fRSJammingSettingsAfrs[1].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsAfrs[2].DeviationCode = 2;
                                fRSJammingSettingsAfrs[2].DurationCode = 2;
                                fRSJammingSettingsAfrs[2].Frequency = 450000; // ?
                                fRSJammingSettingsAfrs[2].Id = 2;
                                fRSJammingSettingsAfrs[2].Liter = 0;
                                fRSJammingSettingsAfrs[2].ManipulationCode = 2;
                                fRSJammingSettingsAfrs[2].ModulationCode = 2;
                                fRSJammingSettingsAfrs[2].Priority = 1;
                                fRSJammingSettingsAfrs[2].Threshold = 130;
                            }

                            //Установка ИРИ ФРЧ для РП, Шифр 6
                            var answerFreqAfrs = await dsp.SetFRSJamming((byte)0, fRSJammingSettingsAfrs);
                            break;

                        case 5:

                            //Установка параметров радиоподавлениЯ для ФРЧ авто, Шифр 54
                            var answerSettingsFrsAuto = await dsp.SetFrsAutoRadioJamSettings(
                                800,
                                10 * 1000,
                                5,
                                20 * 10,
                                -90);

                            Protocols.FRSJammingSetting[] fRSJammingSettingsFrsAuto = new Protocols.FRSJammingSetting[3];
                            {
                                fRSJammingSettingsFrsAuto[0].DeviationCode = 0;
                                fRSJammingSettingsFrsAuto[0].DurationCode = 0;
                                fRSJammingSettingsFrsAuto[0].Frequency = 350000; // ?
                                fRSJammingSettingsFrsAuto[0].Id = 0;
                                fRSJammingSettingsFrsAuto[0].Liter = 0;
                                fRSJammingSettingsFrsAuto[0].ManipulationCode = 0;
                                fRSJammingSettingsFrsAuto[0].ModulationCode = 0;
                                fRSJammingSettingsFrsAuto[0].Priority = 1;
                                fRSJammingSettingsFrsAuto[0].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsFrsAuto[1].DeviationCode = 1;
                                fRSJammingSettingsFrsAuto[1].DurationCode = 1;
                                fRSJammingSettingsFrsAuto[1].Frequency = 400000; // ?
                                fRSJammingSettingsFrsAuto[1].Id = 1;
                                fRSJammingSettingsFrsAuto[1].Liter = 0;
                                fRSJammingSettingsFrsAuto[1].ManipulationCode = 1;
                                fRSJammingSettingsFrsAuto[1].ModulationCode = 1;
                                fRSJammingSettingsFrsAuto[1].Priority = 1;
                                fRSJammingSettingsFrsAuto[1].Threshold = 130;
                            }

                            {
                                fRSJammingSettingsFrsAuto[2].DeviationCode = 2;
                                fRSJammingSettingsFrsAuto[2].DurationCode = 2;
                                fRSJammingSettingsFrsAuto[2].Frequency = 450000; // ?
                                fRSJammingSettingsFrsAuto[2].Id = 2;
                                fRSJammingSettingsFrsAuto[2].Liter = 0;
                                fRSJammingSettingsFrsAuto[2].ManipulationCode = 2;
                                fRSJammingSettingsFrsAuto[2].ModulationCode = 2;
                                fRSJammingSettingsFrsAuto[2].Priority = 1;
                                fRSJammingSettingsFrsAuto[2].Threshold = 130;
                            }

                            //Установка ИРИ ФРЧ для РП, Шифр 6
                            var answerFreqFrsAuto = await dsp.SetFRSJamming((byte)0, fRSJammingSettingsFrsAuto);
                            break;



                        case 6:

                            int duration = 1000;
                            byte bCodeFFT = 4;
                            int N = 1;

                            Protocols.FhssJammingSetting[] fhssJammingSettings = new Protocols.FhssJammingSetting[1];

                            fhssJammingSettings[0].DeviationCode = 0;
                                fhssJammingSettings[0].EndFrequency = 490000;
                                fhssJammingSettings[0].Id = 0;
                                fhssJammingSettings[0].ManipulationCode = 0;
                                fhssJammingSettings[0].ModulationCode = 0;
                                fhssJammingSettings[0].StartFrequency = 350000;
                                fhssJammingSettings[0].Threshold = 80;


                            int len = 0;
                            fhssJammingSettings[0].FixedRadioSourceCount = len;
                            fhssJammingSettings[0].FixedRadioSources = new Protocols.FhssFixedRadioSource[0];
                    

                    //Установка ИРИ ППРЧ для РП, Шифр 7
                    var answerFhss = await dsp.SetFhssJamming(0, duration, bCodeFFT, fhssJammingSettings);
                    break;
                    }
                
                    var answer = await dsp.SetMode(RegimeRadioSuppr + 3);

                }
                catch (SystemException)
                {
                }

            }
        }


        bool spectrum = false;
        private async void Spectrum_Click(object sender, RoutedEventArgs e)
        {
            spectrum = !spectrum;
            if (spectrum)
            {

                pLibrary.Mode = 1;

                pLibrary.СhooseFPS(PanoramaLibrary.PLibrary.FramesPerSecond.FPS30);

                var answer0 = await dsp.SetSectorsAndRanges(0, 0, 25.0d, 100.0d, 0, 360); //Шифр 3
            }
            else
            {
                pLibrary.Mode = 0;
            }
        }

        private async void PLibrary_NeedIntensityRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount, byte TimeLenght)
        {
            var answer = await dsp.GetAmplitudeTimeSpectrum(MinVisibleX, MaxVisibleX, PointCount, TimeLenght);

            if (answer?.Header.ErrorCode == 0)
            {
                pLibrary.IQIntensityPaint(answer.Spectrum, MinVisibleX, MaxVisibleX, PointCount, TimeLenght);
            }
        }

        private async void PLibrary_NeedAdaptiveThresholdRequest(object sender)
        {
            var answer = await dsp.GetAdaptiveThreshold(0);
            if (answer?.Header.ErrorCode == 0)
            {
                pLibrary.AdaptiveThreshold = answer.ThresholdLevel;
            }
        }

        private async void PLibrary_NeedAdaptiveThresholdRequest2(object sender, byte[] Bands)
        {
            var answer = await dsp.GetBandsAdaptiveThreshold(Bands);
            if (answer?.Header.ErrorCode == 0)
            {
                pLibrary.AdaptiveBandsPaint(Bands, answer.Thresholds);
            }
        }

        private async void PLibrary_NeedSpectrumRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            var answer = await dsp.GetSpectrum(MinVisibleX, MaxVisibleX, PointCount);
            if (answer?.Header.ErrorCode == 0)
            {
                if (answer.Spectrum != null)
                {
                    pLibrary.IQSpectrumPainted(MinVisibleX, MaxVisibleX, answer.Spectrum);
                }
            }
        }

        bool bearing = false;
        private async void Spectrum2_Click(object sender, RoutedEventArgs e)
        {
            bearing = !bearing;
            if (bearing)
            {
                pLibrary.Mode = 2;

                pLibrary.СhooseFPS(PanoramaLibrary.PLibrary.FramesPerSecond.FPS30);

                var answer0 = await dsp.SetSectorsAndRanges(0, 0, 25.0d, 100.0d, 0, 360); //Шифр 3
            }
            else
            {
                pLibrary.Mode = 0;
            }
        }

        private async void PLibrary_NeedExBearingRequest(object sender, double MinBandX, double MaxBandX, int PhAvCount, int PlAvCount)
        {
            var answer = await dsp.ExecutiveDF(MinBandX, MaxBandX, PhAvCount, PlAvCount);
            pLibrary.IQExBearingPaited(answer.Direction, answer.CorrelationHistogram, answer.Frequency, answer.StandardDeviation, answer.DiscardedDirectionPercent);
        }

        private async void PLibrary_NeedBearingRequest(object sender, double MinVisibleX, double MaxVisibleX)
        {
            var answer = await dsp.GetBearingPanoramaSignals(MinVisibleX, MaxVisibleX);

            if (answer?.Header.ErrorCode == 0)
            {
                BearingFromDSPPainted(answer);
            }
        }

        //Отрисовка пеленгов
        private void BearingFromDSPPainted(GetBearingPanoramaSignalsResponse data)
        {
            int[] xData = new int[data.FixedSignalsCount];
            int[] yData = new int[data.FixedSignalsCount];

            for (int i = 0; i < data.FixedSignalsCount; i++)
            {
                xData[i] = data.FixedSignals[i].Frequency;
                yData[i] = data.FixedSignals[i].Direction;
            }

            pLibrary.IQBearingPainted(xData, yData);

            xData = new int[data.ImpulseSignalsCount];
            yData = new int[data.ImpulseSignalsCount];

            for (int i = 0; i < data.ImpulseSignalsCount; i++)
            {
                xData[i] = data.ImpulseSignals[i].Frequency;
                yData[i] = data.ImpulseSignals[i].Direction;
            }

            pLibrary.IQBearingPainted(xData, yData);
        }

        bool intens = false;
        private void CB4_Click(object sender, RoutedEventArgs e)
        {
            if (CB4.IsChecked.Value)
            {
                intens = true;
                Task.Run(() => intensRequest());
            }
            else
            {
                intens = false;
            }
        }

        private async void intensRequest()
        {
            while (intens)
            {
                double requestMinFrequency = pLibrary.MinVisibleX;
                double requestMaxFrequency = pLibrary.MaxVisibleX;
                int PointCount = 2000;
                int TimeLength = 60;

                var answer = await dsp.GetAmplitudeTimeSpectrum(requestMinFrequency, requestMaxFrequency, PointCount, (byte)TimeLength);

                if (answer?.Header.ErrorCode == 0)
                {
                    IntensityPaintedNew2(answer, requestMinFrequency, requestMaxFrequency, PointCount, TimeLength);
                }

                await Task.Delay(1000);
            }

        }

        int MinSpectrValue = -130;
        private void IntensityPaintedNew2(GetAmplitudeTimeSpectrumResponse data, double MinFrequency, double MaxFrequency, int NPointCount, int TimeLength)
        {
            double[,] dd = new double[NPointCount, TimeLength];

            //прямой порядок
            int k = 0;
            int count = 0;
            for (int i = 0; i < data.Spectrum.Count(); i++)
            {
                //double temp = (-1) * (double)data.Spectrum[i];
                double temp = MinSpectrValue + data.Spectrum[i];
                //if (temp < porogCursor.YPosition || temp == 0)
                //    temp = -121;
                dd[i - (NPointCount * k), k] = temp;
                count++;
                if (count == NPointCount)
                {
                    count = 0;
                    k++;
                }
            }

            pLibrary.IntensityPaint(MinFrequency, MaxFrequency, dd);
        }

        private void none_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.ClearStorage();
        }

        private void one_Click2(object sender, RoutedEventArgs e)
        {
            pLibrary.GlobalNumberOfBands = 200;
        }

        private void two_Click2(object sender, RoutedEventArgs e)
        {
            pLibrary.GlobalRangeXmin = 100;
        }

        private void three_Click2(object sender, RoutedEventArgs e)
        {
            double[] d = { 25d,35d,55d,65d,3000d,3025d };
            pLibrary.ConvertValueToIndexes(d);

        }

        private void four_Click2(object sender, RoutedEventArgs e)
        {
            //pLibrary.Threshold = 100;

            //pLibrary.ExternalExBearing(50,10);

            pLibrary.SetLanguage("eng");
        }

        private async void five_Click2(object sender, RoutedEventArgs e)
        {

            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, new double[] { 25, 3000 }, new double[] { 55, 3025 });
            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, new double[] { 30, 300 }, new double[] { 40, 400 });

            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, new double[] { 100, 1000 }, new double[] { 500, 2000 });


            var answer = await dsp.SetMode(2);
            pLibrary.Mode = 2;


        }

        private async void six_Click2(object sender, RoutedEventArgs e)
        {
            //pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRS, new double[] { 500, 2000 }, new double[] { 1000, 3000 });
            var answer = await dsp.SetMode(0);
            pLibrary.Mode = 0;
        }

        private async void seven_Click2(object sender, RoutedEventArgs e)
        {
            //byte[] Bands = new byte[100];
            //for (int i = 0; i < Bands.Count(); i++)
            //{
            //    Bands[i] = (byte)i;
            //}
            //var answer = await dsp.GetBandsAdaptiveThreshold(Bands);


            List<double> lFreqStart = new List<double>();
            List<double> lFreqEnd = new List<double>();
            List<double[]> lCutOffFreq = new List<double[]>();
            List<double[]> lCutOffWidth = new List<double[]>();
            List<double> lThreshold = new List<double>();

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(30);
                lFreqEnd.Add(50);

                double[] templCutOffFreq = new double[0];
                double[] templCutOffWidth = new double[0];

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            for (int i = 0; i < 1; i++)
            {
                lFreqStart.Add(70);
                lFreqEnd.Add(100);

                double[] templCutOffFreq = new double[0];
                double[] templCutOffWidth = new double[0];

                lCutOffFreq.Add(templCutOffFreq);
                lCutOffWidth.Add(templCutOffWidth);

                lThreshold.Add(-60);
            }

            pLibrary.FHSSonRS(lFreqStart, lFreqEnd, lCutOffFreq, lCutOffWidth, lThreshold);

            var answer = await dsp.SetMode(6);
            pLibrary.Mode = 6;

        }

        private void Lang_Click(object sender, RoutedEventArgs e)
        {
            pLibrary.SetLanguage("azr");
        }
    }
}
